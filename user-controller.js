const User = require('./user.js')
const md5 = require('md5')

class Usercontroller 
{
    async register(req,res)
    {
        const username = req.body.Username
        const password = md5(req.body.Password)
        const name = req.body.Name
        try {
            User.create({
                Username: username,
                Password: password,
                Name: name
            })
            res.status(200).json({message: 'success'})
        } catch(e) {
            res.status(400).json(e)
        }
        
    }

    async login(req,res)
    {
        try {
            const options = {
                where: {
                    Username: req.body.Username
                }
            }
            const user = await User.findOne(options)
            if(user === null){
                throw new Error('whoops! user tidak ditemukan')
            } else {
               const passwd = md5(req.body.Password)
               const tokens = md5(user.id)
                 if(passwd === user.Password){
                    const result = {
                        message: 'success',
                        token: tokens
                    }
                  res.status(200).json(result)
                } else {
                    throw new Error('whoops! password salah')
                }  
            }
        }
        catch(e) {
            res.status(400).json(e.message)
        }
    }
}

module.exports = new Usercontroller()