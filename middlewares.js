const User = require('./user.js')
const md5 = require('md5')
class Helpers 
{
    async header(req,res,next)
    {
        const auth = req.get('Authorization')
        const user = await User.findAll()
        let check = ''
        req.auth = false
        for(const row of user){
            check = md5(row.id)
            if(check === auth){
                req.auth = true
                break
            }
        }
        next()
    }
}

module.exports = new Helpers()