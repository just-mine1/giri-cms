const Corp = require('./corp.js')
const md5 = require('md5')

class Corpcontroller 
{
     async add(req,res)
     {
        try {
            if(req.auth){
                await Corp.create({
                    Nama: req.body.Nama,
                    Kode: req.body.Kode
                })
                res.status(200).json({message: 'success'})
            } else {
                throw new Error('whoops! user tidak dikenal')
            }
        }
        catch(e) {
            res.status(400).json(e.message)
        }
     }

     async data(req,res)
     {
        try {
            if(req.auth){
            const data = await Corp.findAll()
            const result = {
                message: 'success',
                data: data
            }
            res.status(200).json(result)
            } else {
                throw new Error('whoops! user tidak dikenal')
            }

        }
        catch (e) {
            res.status(400).json(e.message)
        }
     }
}

module.exports = new Corpcontroller()