const Barang = require('./barang.js') 

class Barangcontroller 
{
    async add(req,res)
    {
        try {
            if(req.auth){
                await Barang.create({
                    Nama: req.body.Nama,
                    Harga: req.body.Harga,
                    Stok: req.body.Stok
                })
                res.status(200).json({message: 'success'})
            }
            else {
                throw new Error('whoops! user tidak dikenal')
            }
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    async data(req,res)
    {
        try {
            if(req.auth){
                const data = await Barang.findAll()
                res.status(200).json({message: 'success',data: data})
            } else {
                throw new Error('whoops! user tidak dikenal')
            }
        }
        catch (e) {
            res.status(400).json(e.message)
        }
    }
}

module.exports = new Barangcontroller()