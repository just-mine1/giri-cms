const { DataTypes, Model } = require('sequelize')
const sequelize = require('./sequelize.js')

class User extends Model {}

User.init({
    Name: {
        type: DataTypes.STRING
    },
    Username: {
        type: DataTypes.STRING
    },
    Password: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName: 'user'
})

module.exports = User